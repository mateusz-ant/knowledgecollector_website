from django.contrib import admin


class ApartmentInfoAdmin(admin.ModelAdmin):
    list_display = ('address', 'price', 'surface', 'price_per_surface', 'number_of_rooms')
    list_filter = ['address', 'price', 'surface', 'number_of_rooms']
    search_fields = ['address', 'price', 'surface', 'number_of_rooms']

# admin.site.register(ApartmentInfo, ApartmentInfoAdmin)
