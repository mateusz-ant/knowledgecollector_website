from django.conf.urls import patterns, url
from knowledgecollector import views


urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^search$', views.search_results, name='search')
)

