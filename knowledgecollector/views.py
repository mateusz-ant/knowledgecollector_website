from operator import attrgetter

from urlobject import URLObject
from couchdbkit.ext.django.forms import DocumentForm
from django.shortcuts import render

from knowledgecollector.models import ApartmentInfo


class ApartmentInfoForm(DocumentForm):
    class Meta:
        document = ApartmentInfo


def index(request):
    return render(request, 'knowledgecollector/index.html')


def search_results(request):
    results_per_page = 10

    def get_int(name, default):
        try:
            value = int(request.GET.get(name, default))
            model[name] = value
            return value
        except ValueError:
            return default

    model = {}

    address = request.GET.get('address', '')
    price_min = get_int('price_min', 0)
    price_max = get_int('price_max', 2 ** 31 - 1)
    surface_min = get_int('surface_min', 0)
    surface_max = get_int('surface_max', 2 ** 31 - 1)
    rooms_min = get_int('rooms_min', 0)
    rooms_max = get_int('rooms_max', 2 ** 31 - 1)
    page = get_int('page', 1)
    sort = request.GET.get('sort', 'price')

    apartments = ApartmentInfo.view("knowledgecollector/all", classes={None: ApartmentInfo},
                                    startkey=address,
                                    endkey=address + 'Z')

    filtered_apartments = [apartment for apartment in apartments
                           if price_min <= apartment.price <= price_max
                           and surface_min <= apartment.surface <= surface_max
                           and rooms_min <= apartment.number_of_rooms <= rooms_max]

    if sort in ('price', 'surface', 'number_of_rooms'):
        filtered_apartments = sorted(filtered_apartments, key=attrgetter(sort))

    first_apartment_index = (page - 1) * results_per_page
    last_apartment_index = page * results_per_page

    filtered_apartments = filtered_apartments[first_apartment_index:last_apartment_index]

    url = URLObject(request.get_full_path())

    model['apartments'] = filtered_apartments
    model['address'] = address
    model['form'] = ApartmentInfoForm()
    model['current_page_number'] = page
    model['next_page'] = str(url.query.set_param('page', str(page + 1)))
    model['prev_page'] = str(url.query.set_param('page', str(page - 1)))
    model['sort'] = sort
    model['page_hrefs'] = [str(url.query.set_param('page', str(page_number))) for page_number in
                           xrange(1, int(len(apartments) / results_per_page) + 1)]
    model['page_count'] = len(model['page_hrefs'])

    return render(request, 'knowledgecollector/search.html', model)



