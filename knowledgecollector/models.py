from couchdbkit.ext.django.schema import *


class ApartmentInfo(Document):
    address = StringProperty()
    surface = DecimalProperty()
    price = IntegerProperty()
    phone_number = StringProperty()
    email = StringProperty()
    number_of_rooms = IntegerProperty()
    submission_date = DateProperty()
    # url = StringProperty()

    def price_per_surface(self):
        return int(self.price / self.surface) if self.price and self.surface else 0

    def __unicode__(self):
        return str({'address': self.address,
                    'surface': self.surface,
                    'price': self.price,
                    'phone_number': self.phone_number,
                    'email': self.email,
                    'number_of_rooms': self.number_of_rooms,
                    'submission_date': self.submission_date})
