function (doc) {
    //TODO: Consider splitting with pattern:
    //[^\w\u0104\u0106\u0118\u0141\u0143\u00D3\u015A\u0179\u017B\u0105\u0107\u0119\u0142\u0144\u00F3\u015B\u017A\u017C-]/
    var keywords = doc.address.split(/[,.;]/);
    var dropKeywords = ["ul", "ulica", "al", "aleja", "os", "osiedle", "pow", "powiat", "gm", "gmina"];

    keywords.map(function (keyword) {
        keyword = keyword.trim();
        var lowerKeyword = keyword.toLowerCase();

        if (keyword.length > 0 && dropKeywords.indexOf(lowerKeyword) === -1) {
            emit(keyword, doc);
        }
    });
}
